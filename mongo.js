// WHAT IS MONGODB ATLAS?
		// is a mongodb database in the cloud.It is the cluofd service of MongoDB, the leading noSQL database.

// WHAT IS ROBO3T?
		// is an application which allows the use of GUI to manipulate MongoDB databases. The advantage of Robo3t is it is a local application.

// WHAT IS SHELL?
		// is an ainterface in MongoDB that allows us to input commands and perform CRUD operations in our database

// CRUD OPERATIONS
	// Create, Read , Update, and Delete

	//Create - inserting documents

	/*db.collection.insertOne({

		"fieldA" : "value A",
		"fieldAB" : "value B"

	});  -allow us to insert a document into a collection
	 */


//insertOne({})
	 db.users.insertOne({

	 		"firstName" : "Clark",
	 		"lastName"  : "Reynold",
	 		"age"	: 24,
	 		"contact" : {
	 				"phone" : "09487723814",
	 				"email" : "js@mail.com"

	 		},
	 		"emergencyContact" : "mother"
	 });


//insertMany({})
		/*
			Syntax:
				db.collections.insertMany
			
		*/

		db.users.insertMany([

				{

				"firstName" : "Stephen",
	 			"lastName"  : "Hawking",
	 			"age"	: 76,	
	 			"contact" : {

	 				"phone" : "646454",
	 				"email" : "stepehen@stephen.com"
	 			},

	 			"courses" : [
	 				"Python ", "PhP", "rEACT"
	 			],
	 			"department" : "none"

				},
				{

				"firstName" : "Neil",
	 			"lastName"  : "Armstrong",
	 			"age"	: 36,	
	 			"contact" : {

	 				"phone" : "54545432",
	 				"email" : "neil@neil.com"
	 			},

	 			"courses" : [
	 				"Laravel ", "Sass", "Springboot"
	 			],
	 			"department" : "none"

				},


			])


// Read - find documents'
	/*
			Syntax:
			db.collection.find()
						>>> will return all documents
			db.collection.findOne({})
						>>>will return the first documents
			db.collection.find({"criteria" :" value"})
						>>> will returen all documents that matches the criteria
			db.collection.findOne({"criteria" :" value"})
						>>> will returen first documents that matches the criteria

			db.collection.find({})
	
	*/

	db.users.find()

	db.users.findOne({})

	db.users.find({"firstName" : "Jean"})

	db.users.findOne({"firstName": "Kate"})

	db.users.find({"lastName" : "Armstrong", "age" : "36"})


//Update documents

	/*
		Syntax :
			db.collection.updateOne({"criteria": "value"}), {$set:{"fieldtoBeUpdated": "updatedValue"}}
				>> to update the first item that matches the criteria

			db.collection.updateOne({},{$set:{"fieldtoBeUpdated": "updatedValue"}})
				>> to update the first item in the collection 
		
			db.collection.updateMany({"criteria": "value"},{$set:{"fieldtoBeUpdated": "updatedValue"}})
				>> to update all items that matches the criteria

			db.collection.updateMany({},{$set:{"fieldtoBeUpdated": "updatedValue"}})
				>> to update all items in the collection
	*/


	db.users.updateOne({"firstName" : "John"}, {$set: {"lastName" : "Rey"}})

	db.users.updateOne({},{$set : {"emergencyContact" : "father"}})

	db.users.updateMany({"department" : "none"}, {$set: {"department" : "Tech"}})

	db.users.updateMany({}, {$set: {"emergencyContact" : "mother"}})


// DELETE OPERATION 

		/*  SYNTAX
			
			db.collection.deleteOne({"criteria:"value})
					>> delete the first item that matches the criteria
			db.collection.deleteMany({"criteria" : "value"})
					>> delete ALL items that matches the criteria

			db.collection.deleteOne({})
					>> delete the first item in the collection

			db.collection.deleteMany({})
					>> delete all ITEMS



		*/

	db.users.deleteOne({"firstName": "Neil"})

	db.users.deleteOne({})

	db.users.deleteMany({"lastName" : "Middleton"})

	db.users.deleteMany({})

	














