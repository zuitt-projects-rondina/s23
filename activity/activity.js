db.users.insertMany([
			{
				"firstName" : "Setsuna",
				"lastName" : "Seiei",
				"email" : "setsuna@celestialbeing.mail",
				"password" : "setsuna00",
				"isAdmin" : false
			},
			{
				"firstName" : "Lockon",
				"lastName" : "Stratos",
				"email" : "lockon@celestialbeing.mail",
				"password" : "lockin123",
				"isAdmin" : false
			},
			{
				"firstName" : "Tieria",
				"lastName" : "Erde",
				"email" : "tieria@celestialbeing.mail",
				"password" : "tieria123",
				"isAdmin" : false
			},
			{
				"firstName" : "Allelujah",
				"lastName" : "Haptism",
				"email" : "allelujah@celestialbeing.mail",
				"password" : "",
				"isAdmin" : false
			},
			{
				"firstName" : "Nena",
				"lastName" : "Trinity",
				"email" : "nena@innovators.mail",
				"password" : "nena123",
				"isAdmin" : false
			}

	])

db.courses.insertMany([
			{
				"name" : "Algebra 101",
				"price" : 1500,
				"isActive" : false
			},
			{
				"name" : "Object-Oriented Programming 101",
				"price" : 1000,
				"isActive" : false
			},
			{
				"name" : "Political Science 204",
				"price" : 800,
				"isActive" : false
			}


	])

db.users.find({"isAdmin" : false})

db.users.updateOne({},{$set : {"isAdmin" : true}})

db.courses.updateOne({"name" : "Object-Oriented Programming 101"}, {$set: {"isActive" : true}})

db.courses.deleteMany({"isActive" : false})






